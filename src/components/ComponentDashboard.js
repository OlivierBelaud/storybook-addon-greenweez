import React, { Fragment, useState } from "react";
import { styled, themes, convert } from "@storybook/theming";
import { Icons } from "@storybook/components";

const ListWrapper = styled.div({
  fontSize: 14,
  padding: 0,
  margin: 0,
  overflowX: 'scroll',
});

const Line = styled.div({
  display: "flex",
  "&:hover": {
    background: convert(themes.normal).background.hoverable,
  },
});

const Cell = styled.div({
  display: "flex",
  padding: "5px",
  fontSize: '14px',
  fontWeight: '300',
  minWidth: '220px',
  color: "#333333",
  justifyContent: 'flex-start',
  borderBottom: `1px solid ${convert(themes.normal).appBorderColor}`,
  fontFamily: '"Nunito Sans",-apple-system,".SFNSText-Regular","San Francisco",BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif;'
});

const CellFirst = styled(Cell)({
  position: 'sticky',
  left: 0,
  zIndex: 2,
  minWidth: '220px',
  backgroundColor: '#fff',
  justifyContent: 'space-between',
  borderRight: `1px solid ${convert(themes.normal).appBorderColor}`,
});

const CellGroup = styled.div({
  // position: 'absolute',
  // left: '300px',
  // width: '100%',
  display: 'flex',
});

const OverFlowScroll = styled.div({
  // overflowX: 'scroll',
  // display: 'flex',
  // width: '100%',
});

const Icon = styled(Icons)({
  height: 10,
  width: 10,
  minWidth: 10,
  marginLeft: 8,
  color: convert(themes.normal).color.mediumdark,
  marginRight: 10,
  transition: "transform 0.1s ease-in-out",
  alignSelf: "center",
  display: "inline-flex",
});

const IconClick = styled(Icon)({
  cursor: "pointer"
});

const Description = styled.div({
  padding: convert(themes.normal).layoutMargin,
  fontSize: '14px',
  fontWeight: '300',
  borderBottom: `1px solid ${convert(themes.normal).appBorderColor}`,
  fontFamily: '"Nunito Sans",-apple-system,".SFNSText-Regular","San Francisco",BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif;'
});

const CodeFormatter = styled.pre({
  backgroundColor: '#f4f4f4',
  border: '1px solid #ddd',
  borderLeft: '3px solid #f2334c',
  color: '#666',
  pageBreakInside: 'avoid',
  fontFamily: 'monospace',
  fontSize: '12px',
  lineHeight: '1.6',
  maxWidth: '100%',
  overflow: 'auto',
  padding: '1em 1.5em',
  display: 'block',
  wordWrap: 'break-word'
})

export const ListItem = ({ item }) => {
  const [open, onToggle] = useState(false);
  const [tab, changeTab] = useState(null);

  const clickTab = (tabName) => {
    console.log('before', open, tab)
    if(tab === tabName) {
      onToggle(false)
      changeTab(null)
    }else{
      changeTab(tabName)
      onToggle(true)
    }
    console.log('new', open, tab)
  }

  return (
    <Fragment>
      <Line>
        <CellFirst>
            <a href={`?path=/docs/ui-${item.name.toLowerCase()}--default-story`}>{item.name}</a>
            <IconClick
              onClick={() => clickTab('code')}
              icon="markup"
              size={10}
              color={convert(themes.normal).appBorderColor}
            />
        </CellFirst>
        <Cell>{item.type}</Cell>
        <Cell>
          {item.componentsNb > 0 ? <div style={{display: 'flex'}}>
            <div>{item.componentsNb}</div>
            <IconClick
              onClick={() => clickTab('comp')}
              icon="eye"
              size={10}
              color={convert(themes.normal).appBorderColor}
            />
          </div> : null
          }
        </Cell>
        <Cell>
          {item.directUseNb > 0 ? <div style={{display: 'flex'}}>
            <div>{item.directUseNb}</div>
            <IconClick
              onClick={() => clickTab('direct')}
              icon="eye"
              size={10}
              color={convert(themes.normal).appBorderColor}
            />
          </div> : null
          }
        </Cell>
        <Cell>{item.slot ? 1 : ''}</Cell>
        <Cell>{item.name}</Cell>
      </Line>
      {open ? <Description>
        {(tab === 'code') ? <CodeFormatter>
            <code>
              {item.code}
            </code>
          </CodeFormatter> : null}
        {(tab === 'comp') ? <div>
          {item.components.map((component, idx) => (
            <div key={idx}>{component}</div>
          ))}
        </div> : null}
        {(tab === 'direct') ? <div>
          {item.directUse.map((use, idx) => (
            <div key={idx}>{use.type}: {use.name}</div>
          ))}
        </div> : null}
      </Description> : null}
    </Fragment>
  );
};

export const ComponentDashboard = ({ items }) => (
  <ListWrapper>
    <Line>
      <CellFirst>Name</CellFirst>
      <Cell>Type</Cell>
      <Cell>Components</Cell>
      <Cell>Used by</Cell>
      <Cell>Slot</Cell>
      <Cell>Name</Cell>
    </Line>
    {items.map((item, idx) => (
      <ListItem key={idx} item={item}></ListItem>
    ))}
  </ListWrapper>
);
