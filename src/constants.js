export const ADDON_ID = "storybook/systemui";
export const TOOL_ID = `${ADDON_ID}/tool`;
export const PANEL_ID = `${ADDON_ID}/panel`;
export const TAB_ID_USED = `${ADDON_ID}/tab_used`;
export const TAB_ID_TESTS = `${ADDON_ID}/tab_tests`;
export const TAB_ID_CODE = `${ADDON_ID}/tab_code`;
export const PARAM_KEY = `myAddonParameter`;

export const EVENTS = {
  RESULT: `${ADDON_ID}/result`,
  REQUEST: `${ADDON_ID}/request`,
  CLEAR: `${ADDON_ID}/clear`,
};
