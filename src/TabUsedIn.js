import React from "react";
import { useParameter } from "@storybook/api";
import { PARAM_KEY } from "./constants";
import { TabContentUsedIn } from "./components/TabContentUsedIn";
import { useStorybookState } from '@storybook/api';
import repository from '../../../.storybook/json/repository.json'

export const TabUsedIn = ({ active }) => {
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  const paramData = useParameter(PARAM_KEY, []);

  const state = useStorybookState();
  // console.log('state', state)

  const { storyId } = useStorybookState();
  const componentName = storyId?.split('--')[0].split('-').reverse()[0]
  console.log('componentName', componentName)

  const datas = repository.find(elem => elem.name.toLowerCase() === componentName) || repository.find(elem => elem.name.substring(1).toLowerCase() === componentName)

  if(datas){
    return active ? <TabContentUsedIn code={datas} /> : null;
  }

  return (<div>No Datas</div>)
};
