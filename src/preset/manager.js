import { addons, types } from "@storybook/addons";

import { ADDON_ID, TAB_ID_USED, TAB_ID_TESTS, TAB_ID_CODE, TOOL_ID } from "../constants";
import { Tool } from "../Tool";
// import { Panel } from "../Panel";
import { TabUsedIn } from "../TabUsedIn";
import { TabTests } from "../TabTests";
import { TabCode } from "../TabCode";

// Register the addon
addons.register(ADDON_ID, () => {
  // Register the tool
  addons.add(TOOL_ID, {
    type: types.TOOL,
    title: "My addon",
    match: ({ viewMode }) => !!(viewMode && viewMode.match(/^(story|docs)$/)),
    render: Tool,
  });

  //Register the panel
  // addons.add(PANEL_ID, {
  //   type: types.PANEL,
  //   title: "My addon",
  //   match: ({ viewMode }) => viewMode === "story",
  //   render: Panel,
  // });

  // Register the tab
  addons.add(TAB_ID_USED, {
    type: types.TAB,
    title: "Used in",
    //👇 Checks the current route for the story
    route: ({ storyId }) => `/used/${storyId}`,
    //👇 Shows the Tab UI element in myaddon view mode
    match: ({ viewMode }) => viewMode === "used",
    render: TabUsedIn,
  });

  addons.add(TAB_ID_TESTS, {
    type: types.TAB,
    title: "Tests",
    //👇 Checks the current route for the story
    route: ({ storyId }) => `/tests/${storyId}`,
    //👇 Shows the Tab UI element in myaddon view mode
    match: ({ viewMode }) => viewMode === "tests",
    render: TabTests,
  });

  addons.add(TAB_ID_CODE, {
    type: types.TAB,
    title: "Code",
    //👇 Checks the current route for the story
    route: ({ storyId }) => `/code/${storyId}`,
    //👇 Shows the Tab UI element in myaddon view mode
    match: ({ viewMode }) => viewMode === "code",
    render: TabCode,
  });


});
