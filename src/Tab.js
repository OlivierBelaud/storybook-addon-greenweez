import React from "react";
import { useParameter } from "@storybook/api";
import { PARAM_KEY } from "./constants";
import { TabContent } from "./components/TabContent";
import { useStorybookState } from '@storybook/api';
import { useStorybookApi } from '@storybook/api';

export const Tab = ({ active }) => {
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  const paramData = useParameter(PARAM_KEY, []);

  console.log('coucou')
  const { viewMode, storyId, storiesHash } = useStorybookState();
  console.log('state', viewMode, storyId, storiesHash)
  const hash = storiesHash[storyId]
  console.log('hash', hash?.parameters?.fileName)

  // console.log('active', active)
  // console.log('paramData', paramData)

  return active ? <TabContent code={paramData} /> : null;
};
