import React from "react";
import { useParameter } from "@storybook/api";
import { PARAM_KEY } from "./constants";
import { TabContentCode } from "./components/TabContentCode";
import { useStorybookState } from '@storybook/api';

export const TabCode = ({ active }) => {
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  const paramData = useParameter(PARAM_KEY, []);

  // console.log('coucou')
  // const { viewMode, storyId, storiesHash } = useStorybookState();
  // console.log('state', viewMode, storyId, storiesHash)
  // const hash = storiesHash[storyId]
  // console.log('hash', hash?.parameters?.fileName)

  // console.log('active', active)
  // console.log('paramData', paramData)

  return active ? <TabContentCode code={paramData} /> : null;
};
