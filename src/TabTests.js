import React from "react";
import { useParameter } from "@storybook/api";
import { PARAM_KEY } from "./constants";
import { TabContentTests } from "./components/TabContentTests";
import { useStorybookState } from '@storybook/api';

import cypress from '../../../.storybook/json/tests.json'
console.log('cypress', cypress)

export const TabTests = ({ active }) => {
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  const paramData = useParameter(PARAM_KEY, []);

  const state = useStorybookState();
  console.log('state', state)

  const { storyId } = useStorybookState();
  const componentName = storyId?.split('--')[0].split('-').reverse()[0]
  console.log('componentName', componentName)

  const datas = cypress?.results.find(elem => elem.file.split('/').reverse()[0].split('.')[0].toLowerCase() === componentName)

  if(datas){
    return active ? <TabContentTests code={datas} /> : null;
  }

  return (<div>No Datas</div>)
};
