"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabTests = void 0;

var _react = _interopRequireDefault(require("react"));

var _api = require("@storybook/api");

var _constants = require("./constants");

var _TabContentTests = require("./components/TabContentTests");

var _tests = _interopRequireDefault(require("../../../.storybook/json/tests.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

console.log('cypress', _tests["default"]);

var TabTests = function TabTests(_ref) {
  var active = _ref.active;
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  var paramData = (0, _api.useParameter)(_constants.PARAM_KEY, []);
  var state = (0, _api.useStorybookState)();
  console.log('state', state);

  var _useStorybookState = (0, _api.useStorybookState)(),
      storyId = _useStorybookState.storyId;

  var componentName = storyId === null || storyId === void 0 ? void 0 : storyId.split('--')[0].split('-').reverse()[0];
  console.log('componentName', componentName);
  var datas = _tests["default"] === null || _tests["default"] === void 0 ? void 0 : _tests["default"].results.find(function (elem) {
    return elem.file.split('/').reverse()[0].split('.')[0].toLowerCase() === componentName;
  });

  if (datas) {
    return active ? /*#__PURE__*/_react["default"].createElement(_TabContentTests.TabContentTests, {
      code: datas
    }) : null;
  }

  return /*#__PURE__*/_react["default"].createElement("div", null, "No Datas");
};

exports.TabTests = TabTests;