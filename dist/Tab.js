"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Tab = void 0;

var _react = _interopRequireDefault(require("react"));

var _api = require("@storybook/api");

var _constants = require("./constants");

var _TabContent = require("./components/TabContent");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Tab = function Tab(_ref) {
  var _hash$parameters;

  var active = _ref.active;
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  var paramData = (0, _api.useParameter)(_constants.PARAM_KEY, []);
  console.log('coucou');

  var _useStorybookState = (0, _api.useStorybookState)(),
      viewMode = _useStorybookState.viewMode,
      storyId = _useStorybookState.storyId,
      storiesHash = _useStorybookState.storiesHash;

  console.log('state', viewMode, storyId, storiesHash);
  var hash = storiesHash[storyId];
  console.log('hash', hash === null || hash === void 0 ? void 0 : (_hash$parameters = hash.parameters) === null || _hash$parameters === void 0 ? void 0 : _hash$parameters.fileName); // console.log('active', active)
  // console.log('paramData', paramData)

  return active ? /*#__PURE__*/_react["default"].createElement(_TabContent.TabContent, {
    code: paramData
  }) : null;
};

exports.Tab = Tab;