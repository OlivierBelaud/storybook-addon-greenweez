"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabUsedIn = void 0;

var _react = _interopRequireDefault(require("react"));

var _api = require("@storybook/api");

var _constants = require("./constants");

var _TabContentUsedIn = require("./components/TabContentUsedIn");

var _repository = _interopRequireDefault(require("../../../.storybook/json/repository.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TabUsedIn = function TabUsedIn(_ref) {
  var active = _ref.active;
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  var paramData = (0, _api.useParameter)(_constants.PARAM_KEY, []);
  var state = (0, _api.useStorybookState)(); // console.log('state', state)

  var _useStorybookState = (0, _api.useStorybookState)(),
      storyId = _useStorybookState.storyId;

  var componentName = storyId === null || storyId === void 0 ? void 0 : storyId.split('--')[0].split('-').reverse()[0];
  console.log('componentName', componentName);

  var datas = _repository["default"].find(function (elem) {
    return elem.name.toLowerCase() === componentName;
  }) || _repository["default"].find(function (elem) {
    return elem.name.substring(1).toLowerCase() === componentName;
  });

  if (datas) {
    return active ? /*#__PURE__*/_react["default"].createElement(_TabContentUsedIn.TabContentUsedIn, {
      code: datas
    }) : null;
  }

  return /*#__PURE__*/_react["default"].createElement("div", null, "No Datas");
};

exports.TabUsedIn = TabUsedIn;