"use strict";

var _addons = require("@storybook/addons");

var _constants = require("../constants");

var _Tool = require("../Tool");

var _TabUsedIn = require("../TabUsedIn");

var _TabTests = require("../TabTests");

var _TabCode = require("../TabCode");

// import { Panel } from "../Panel";
// Register the addon
_addons.addons.register(_constants.ADDON_ID, function () {
  // Register the tool
  _addons.addons.add(_constants.TOOL_ID, {
    type: _addons.types.TOOL,
    title: "My addon",
    match: function match(_ref) {
      var viewMode = _ref.viewMode;
      return !!(viewMode && viewMode.match(/^(story|docs)$/));
    },
    render: _Tool.Tool
  }); //Register the panel
  // addons.add(PANEL_ID, {
  //   type: types.PANEL,
  //   title: "My addon",
  //   match: ({ viewMode }) => viewMode === "story",
  //   render: Panel,
  // });
  // Register the tab


  _addons.addons.add(_constants.TAB_ID_USED, {
    type: _addons.types.TAB,
    title: "Used in",
    //👇 Checks the current route for the story
    route: function route(_ref2) {
      var storyId = _ref2.storyId;
      return "/used/".concat(storyId);
    },
    //👇 Shows the Tab UI element in myaddon view mode
    match: function match(_ref3) {
      var viewMode = _ref3.viewMode;
      return viewMode === "used";
    },
    render: _TabUsedIn.TabUsedIn
  });

  _addons.addons.add(_constants.TAB_ID_TESTS, {
    type: _addons.types.TAB,
    title: "Tests",
    //👇 Checks the current route for the story
    route: function route(_ref4) {
      var storyId = _ref4.storyId;
      return "/tests/".concat(storyId);
    },
    //👇 Shows the Tab UI element in myaddon view mode
    match: function match(_ref5) {
      var viewMode = _ref5.viewMode;
      return viewMode === "tests";
    },
    render: _TabTests.TabTests
  });

  _addons.addons.add(_constants.TAB_ID_CODE, {
    type: _addons.types.TAB,
    title: "Code",
    //👇 Checks the current route for the story
    route: function route(_ref6) {
      var storyId = _ref6.storyId;
      return "/code/".concat(storyId);
    },
    //👇 Shows the Tab UI element in myaddon view mode
    match: function match(_ref7) {
      var viewMode = _ref7.viewMode;
      return viewMode === "code";
    },
    render: _TabCode.TabCode
  });
});