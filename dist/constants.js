"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EVENTS = exports.PARAM_KEY = exports.TAB_ID_CODE = exports.TAB_ID_TESTS = exports.TAB_ID_USED = exports.PANEL_ID = exports.TOOL_ID = exports.ADDON_ID = void 0;
var ADDON_ID = "storybook/systemui";
exports.ADDON_ID = ADDON_ID;
var TOOL_ID = "".concat(ADDON_ID, "/tool");
exports.TOOL_ID = TOOL_ID;
var PANEL_ID = "".concat(ADDON_ID, "/panel");
exports.PANEL_ID = PANEL_ID;
var TAB_ID_USED = "".concat(ADDON_ID, "/tab_used");
exports.TAB_ID_USED = TAB_ID_USED;
var TAB_ID_TESTS = "".concat(ADDON_ID, "/tab_tests");
exports.TAB_ID_TESTS = TAB_ID_TESTS;
var TAB_ID_CODE = "".concat(ADDON_ID, "/tab_code");
exports.TAB_ID_CODE = TAB_ID_CODE;
var PARAM_KEY = "myAddonParameter";
exports.PARAM_KEY = PARAM_KEY;
var EVENTS = {
  RESULT: "".concat(ADDON_ID, "/result"),
  REQUEST: "".concat(ADDON_ID, "/request"),
  CLEAR: "".concat(ADDON_ID, "/clear")
};
exports.EVENTS = EVENTS;