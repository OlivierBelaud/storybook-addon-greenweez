"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComponentDashboard = exports.ListItem = void 0;

var _react = _interopRequireWildcard(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr && (typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]); if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var ListWrapper = _theming.styled.div({
  fontSize: 14,
  padding: 0,
  margin: 0,
  overflowX: 'scroll'
});

var Line = _theming.styled.div({
  display: "flex",
  "&:hover": {
    background: (0, _theming.convert)(_theming.themes.normal).background.hoverable
  }
});

var Cell = _theming.styled.div({
  display: "flex",
  padding: "5px",
  fontSize: '14px',
  fontWeight: '300',
  minWidth: '220px',
  color: "#333333",
  justifyContent: 'flex-start',
  borderBottom: "1px solid ".concat((0, _theming.convert)(_theming.themes.normal).appBorderColor),
  fontFamily: '"Nunito Sans",-apple-system,".SFNSText-Regular","San Francisco",BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif;'
});

var CellFirst = (0, _theming.styled)(Cell)({
  position: 'sticky',
  left: 0,
  zIndex: 2,
  minWidth: '220px',
  backgroundColor: '#fff',
  justifyContent: 'space-between',
  borderRight: "1px solid ".concat((0, _theming.convert)(_theming.themes.normal).appBorderColor)
});

var CellGroup = _theming.styled.div({
  // position: 'absolute',
  // left: '300px',
  // width: '100%',
  display: 'flex'
});

var OverFlowScroll = _theming.styled.div({// overflowX: 'scroll',
  // display: 'flex',
  // width: '100%',
});

var Icon = (0, _theming.styled)(_components.Icons)({
  height: 10,
  width: 10,
  minWidth: 10,
  marginLeft: 8,
  color: (0, _theming.convert)(_theming.themes.normal).color.mediumdark,
  marginRight: 10,
  transition: "transform 0.1s ease-in-out",
  alignSelf: "center",
  display: "inline-flex"
});
var IconClick = (0, _theming.styled)(Icon)({
  cursor: "pointer"
});

var Description = _theming.styled.div({
  padding: (0, _theming.convert)(_theming.themes.normal).layoutMargin,
  fontSize: '14px',
  fontWeight: '300',
  borderBottom: "1px solid ".concat((0, _theming.convert)(_theming.themes.normal).appBorderColor),
  fontFamily: '"Nunito Sans",-apple-system,".SFNSText-Regular","San Francisco",BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif;'
});

var CodeFormatter = _theming.styled.pre({
  backgroundColor: '#f4f4f4',
  border: '1px solid #ddd',
  borderLeft: '3px solid #f2334c',
  color: '#666',
  pageBreakInside: 'avoid',
  fontFamily: 'monospace',
  fontSize: '12px',
  lineHeight: '1.6',
  maxWidth: '100%',
  overflow: 'auto',
  padding: '1em 1.5em',
  display: 'block',
  wordWrap: 'break-word'
});

var ListItem = function ListItem(_ref) {
  var item = _ref.item;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      onToggle = _useState2[1];

  var _useState3 = (0, _react.useState)(null),
      _useState4 = _slicedToArray(_useState3, 2),
      tab = _useState4[0],
      changeTab = _useState4[1];

  var clickTab = function clickTab(tabName) {
    console.log('before', open, tab);

    if (tab === tabName) {
      onToggle(false);
      changeTab(null);
    } else {
      changeTab(tabName);
      onToggle(true);
    }

    console.log('new', open, tab);
  };

  return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement(Line, null, /*#__PURE__*/_react["default"].createElement(CellFirst, null, /*#__PURE__*/_react["default"].createElement("a", {
    href: "?path=/docs/ui-".concat(item.name.toLowerCase(), "--default-story")
  }, item.name), /*#__PURE__*/_react["default"].createElement(IconClick, {
    onClick: function onClick() {
      return clickTab('code');
    },
    icon: "markup",
    size: 10,
    color: (0, _theming.convert)(_theming.themes.normal).appBorderColor
  })), /*#__PURE__*/_react["default"].createElement(Cell, null, item.type), /*#__PURE__*/_react["default"].createElement(Cell, null, item.componentsNb > 0 ? /*#__PURE__*/_react["default"].createElement("div", {
    style: {
      display: 'flex'
    }
  }, /*#__PURE__*/_react["default"].createElement("div", null, item.componentsNb), /*#__PURE__*/_react["default"].createElement(IconClick, {
    onClick: function onClick() {
      return clickTab('comp');
    },
    icon: "eye",
    size: 10,
    color: (0, _theming.convert)(_theming.themes.normal).appBorderColor
  })) : null), /*#__PURE__*/_react["default"].createElement(Cell, null, item.directUseNb > 0 ? /*#__PURE__*/_react["default"].createElement("div", {
    style: {
      display: 'flex'
    }
  }, /*#__PURE__*/_react["default"].createElement("div", null, item.directUseNb), /*#__PURE__*/_react["default"].createElement(IconClick, {
    onClick: function onClick() {
      return clickTab('direct');
    },
    icon: "eye",
    size: 10,
    color: (0, _theming.convert)(_theming.themes.normal).appBorderColor
  })) : null), /*#__PURE__*/_react["default"].createElement(Cell, null, item.slot ? 1 : ''), /*#__PURE__*/_react["default"].createElement(Cell, null, item.name)), open ? /*#__PURE__*/_react["default"].createElement(Description, null, tab === 'code' ? /*#__PURE__*/_react["default"].createElement(CodeFormatter, null, /*#__PURE__*/_react["default"].createElement("code", null, item.code)) : null, tab === 'comp' ? /*#__PURE__*/_react["default"].createElement("div", null, item.components.map(function (component, idx) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      key: idx
    }, component);
  })) : null, tab === 'direct' ? /*#__PURE__*/_react["default"].createElement("div", null, item.directUse.map(function (use, idx) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      key: idx
    }, use.type, ": ", use.name);
  })) : null) : null);
};

exports.ListItem = ListItem;

var ComponentDashboard = function ComponentDashboard(_ref2) {
  var items = _ref2.items;
  return /*#__PURE__*/_react["default"].createElement(ListWrapper, null, /*#__PURE__*/_react["default"].createElement(Line, null, /*#__PURE__*/_react["default"].createElement(CellFirst, null, "Name"), /*#__PURE__*/_react["default"].createElement(Cell, null, "Type"), /*#__PURE__*/_react["default"].createElement(Cell, null, "Components"), /*#__PURE__*/_react["default"].createElement(Cell, null, "Used by"), /*#__PURE__*/_react["default"].createElement(Cell, null, "Slot"), /*#__PURE__*/_react["default"].createElement(Cell, null, "Name")), items.map(function (item, idx) {
    return /*#__PURE__*/_react["default"].createElement(ListItem, {
      key: idx,
      item: item
    });
  }));
};

exports.ComponentDashboard = ComponentDashboard;