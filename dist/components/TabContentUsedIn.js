"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabContentUsedIn = void 0;

var _react = _interopRequireDefault(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//import student from path2
var TabWrapper = _theming.styled.div(function (_ref) {
  var theme = _ref.theme;
  return {
    background: theme.background.content,
    padding: "4rem 20px",
    minHeight: "100vh",
    boxSizing: "border-box"
  };
});

var TabInner = _theming.styled.div(function (_ref2) {
  var theme = _ref2.theme;
  return {
    maxWidth: 768,
    marginLeft: "auto",
    marginRight: "auto"
  };
});

var TabContentUsedIn = function TabContentUsedIn(_ref3) {
  var code = _ref3.code;
  //console.log('student', student)
  return /*#__PURE__*/_react["default"].createElement(TabWrapper, null, /*#__PURE__*/_react["default"].createElement(TabInner, null, /*#__PURE__*/_react["default"].createElement(_components.Title, null, "Used In"), /*#__PURE__*/_react["default"].createElement("div", null, code.name), /*#__PURE__*/_react["default"].createElement("div", null, "Direct use: ", code.directUseNb), code.directUse.map(function (item, idx) {
    return /*#__PURE__*/_react["default"].createElement("div", {
      key: idx
    }, /*#__PURE__*/_react["default"].createElement("div", null, item.path, " / type: ", item.type), /*#__PURE__*/_react["default"].createElement("div", null, "ui-".concat(item.name.toLowerCase(), "--default-story")), /*#__PURE__*/_react["default"].createElement(_components.Preview, null, /*#__PURE__*/_react["default"].createElement(_components.Story, {
      id: "ui-".concat(item.name.toLowerCase(), "--default-story")
    })));
  })));
};

exports.TabContentUsedIn = TabContentUsedIn;