"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabContent = void 0;

var _react = _interopRequireDefault(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TabWrapper = _theming.styled.div(function (_ref) {
  var theme = _ref.theme;
  return {
    background: theme.background.content,
    padding: "4rem 20px",
    minHeight: "100vh",
    boxSizing: "border-box"
  };
});

var TabInner = _theming.styled.div(function (_ref2) {
  var theme = _ref2.theme;
  return {
    maxWidth: 768,
    marginLeft: "auto",
    marginRight: "auto"
  };
});

var TabContent = function TabContent(_ref3) {
  var code = _ref3.code;
  return /*#__PURE__*/_react["default"].createElement(TabWrapper, null, /*#__PURE__*/_react["default"].createElement(TabInner, null, /*#__PURE__*/_react["default"].createElement(_components.Title, null, "Test 2"), /*#__PURE__*/_react["default"].createElement("p", null, "Your addon can create a custom tab in Storybook. For example, the official", " ", /*#__PURE__*/_react["default"].createElement(_components.Link, {
    href: "https://storybook.js.org/docs/react/writing-docs/introduction"
  }, "@storybook/addon-docs"), " ", "uses this pattern."), /*#__PURE__*/_react["default"].createElement("p", null, "You have full control over what content is being rendered here. You can use components from", " ", /*#__PURE__*/_react["default"].createElement(_components.Link, {
    href: "https://github.com/storybookjs/storybook/tree/master/lib/components"
  }, "@storybook/components"), " ", "to match the look and feel of Storybook, for example the", " ", /*#__PURE__*/_react["default"].createElement("code", null, "<Source />"), " component below. Or build a completely custom UI."), /*#__PURE__*/_react["default"].createElement(_components.Source, {
    code: code,
    language: "jsx",
    format: false
  })));
};

exports.TabContent = TabContent;