"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabContentCode = void 0;

var _react = _interopRequireDefault(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TabWrapper = _theming.styled.div(function (_ref) {
  var theme = _ref.theme;
  return {
    background: theme.background.content,
    padding: "4rem 20px",
    minHeight: "100vh",
    boxSizing: "border-box"
  };
});

var TabInner = _theming.styled.div(function (_ref2) {
  var theme = _ref2.theme;
  return {
    maxWidth: 768,
    marginLeft: "auto",
    marginRight: "auto"
  };
});

var TabContentCode = function TabContentCode(_ref3) {
  var code = _ref3.code;
  return /*#__PURE__*/_react["default"].createElement(TabWrapper, null, /*#__PURE__*/_react["default"].createElement(TabInner, null, /*#__PURE__*/_react["default"].createElement(_components.Title, null, "Code")));
};

exports.TabContentCode = TabContentCode;