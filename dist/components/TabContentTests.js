"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabContentTests = void 0;

var _react = _interopRequireDefault(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TabWrapper = _theming.styled.div(function (_ref) {
  var theme = _ref.theme;
  return {
    background: theme.background.content,
    padding: "4rem 20px",
    minHeight: "100vh",
    boxSizing: "border-box"
  };
});

var TabInner = _theming.styled.div(function (_ref2) {
  var theme = _ref2.theme;
  return {
    maxWidth: 768,
    marginLeft: "auto",
    marginRight: "auto"
  };
});

var TabContentTests = function TabContentTests(_ref3) {
  var code = _ref3.code;
  return /*#__PURE__*/_react["default"].createElement(TabWrapper, null, /*#__PURE__*/_react["default"].createElement(TabInner, null, /*#__PURE__*/_react["default"].createElement(_components.Title, null, "Tests"), /*#__PURE__*/_react["default"].createElement("div", null, "Nb de suite: ", code.suites.length), code.suites.map(function (suite, idx) {
    return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
      key: idx
    }, "Suite ", suite.title, ": ", suite.tests.length, " tests"), /*#__PURE__*/_react["default"].createElement("div", null, suite.tests.map(function (test, id) {
      return /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
        key: "".concat(idx, "-").concat(id)
      }, "- ", test.title, ": ", test.pass ? 'ok' : 'ko'));
    })));
  })));
};

exports.TabContentTests = TabContentTests;