"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PanelContent = exports.RequestDataButton = void 0;

var _react = _interopRequireWildcard(require("react"));

var _theming = require("@storybook/theming");

var _components = require("@storybook/components");

var _List = require("./List");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var RequestDataButton = (0, _theming.styled)(_components.Button)({
  marginTop: "1rem"
});
/**
 * Checkout https://github.com/storybookjs/storybook/blob/next/addons/jest/src/components/Panel.tsx
 * for a real world example
 */

exports.RequestDataButton = RequestDataButton;

var PanelContent = function PanelContent(_ref) {
  var results = _ref.results,
      fetchData = _ref.fetchData,
      clearData = _ref.clearData;
  return /*#__PURE__*/_react["default"].createElement(_components.TabsState, {
    initial: "overview",
    backgroundColor: (0, _theming.convert)(_theming.themes.normal).background.hoverable
  }, /*#__PURE__*/_react["default"].createElement("div", {
    id: "overview",
    title: "Overview",
    color: (0, _theming.convert)(_theming.themes.normal).color.positive
  }, /*#__PURE__*/_react["default"].createElement(_components.Placeholder, null, /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, "Addons can gather details about how a story is rendered. This is panel uses a tab pattern. Click the button below to fetch data for the other two tabs."), /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement(RequestDataButton, {
    secondary: true,
    small: true,
    onClick: fetchData,
    style: {
      marginRight: 16
    }
  }, "Request data"), /*#__PURE__*/_react["default"].createElement(RequestDataButton, {
    outline: true,
    small: true,
    onClick: clearData
  }, "Clear data")))), /*#__PURE__*/_react["default"].createElement("div", {
    id: "danger",
    title: "".concat(results.danger.length, " Danger"),
    color: (0, _theming.convert)(_theming.themes.normal).color.negative
  }, /*#__PURE__*/_react["default"].createElement(_List.List, {
    items: results.danger
  })), /*#__PURE__*/_react["default"].createElement("div", {
    id: "warning",
    title: "".concat(results.warning.length, " Warning"),
    color: (0, _theming.convert)(_theming.themes.normal).color.warning
  }, /*#__PURE__*/_react["default"].createElement(_List.List, {
    items: results.warning
  })));
};

exports.PanelContent = PanelContent;