"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabCode = void 0;

var _react = _interopRequireDefault(require("react"));

var _api = require("@storybook/api");

var _constants = require("./constants");

var _TabContentCode = require("./components/TabContentCode");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TabCode = function TabCode(_ref) {
  var active = _ref.active;
  // https://storybook.js.org/docs/react/addons/addons-api#useparameter
  var paramData = (0, _api.useParameter)(_constants.PARAM_KEY, []); // console.log('coucou')
  // const { viewMode, storyId, storiesHash } = useStorybookState();
  // console.log('state', viewMode, storyId, storiesHash)
  // const hash = storiesHash[storyId]
  // console.log('hash', hash?.parameters?.fileName)
  // console.log('active', active)
  // console.log('paramData', paramData)

  return active ? /*#__PURE__*/_react["default"].createElement(_TabContentCode.TabContentCode, {
    code: paramData
  }) : null;
};

exports.TabCode = TabCode;