# Requirement

### CLI

Command Line Interface to scan the folder et agregates all the datas available about the repository

-> command in a context

### StoryBook Components Lib

Components Library to augment the capacity of writing stories in mdx
Those components can be imported in the project as a standard lib component and be used in mdx

-> component importation

### Addon : New Tabs and Components

New tabs to add on storybook to augment the storybook configuration
Those element change the conf of storybook and add new page connected to data with the context
The context of the page (which component) will filter the datas to get only the data concerned and populate the default page components

-> addon registration


## Tests strategie

- Check for props testing (que se passe t'il si je passe les mauvaise props)
Dans le cas où il y a une logique de props à mettre en place avec une sorte de validator

- Check component display
Est ce que je veux que mon composant s'affiche coûte que coûte, dans quel cas je ne veux pas que celui-ci s'affiche

- Check part of the component dispaly
Pareil mais sur une partie du composant